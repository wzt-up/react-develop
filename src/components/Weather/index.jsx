import React, { Component } from "react";
import { Descriptions } from "antd";
import "./index.scss";
export default class Weather extends Component {
  render() {
    const { weather } = this.props;
    return (
      <div className="weather">
        <Descriptions title="weather card">
          <Descriptions.Item label="城市">
            {weather.province}-{weather.city}
          </Descriptions.Item>
          <Descriptions.Item label="时间">
            {weather.reporttime}
          </Descriptions.Item>
          <Descriptions.Item label="温度">
            {weather.temperature}
          </Descriptions.Item>
          <Descriptions.Item label="天气">{weather.weather}</Descriptions.Item>
          <Descriptions.Item label="风力风向">
            {weather.winddirection}风{weather.windpower}级
          </Descriptions.Item>
        </Descriptions>
      </div>
    );
  }
}
