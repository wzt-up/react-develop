import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "../pages/home/index";
import Account from "../pages/account/index";
import Login from "../pages/login";
import Antd from "../pages/antd-view";
import LifeCycles from "../pages/lifecycles";
import ElementUi from "../pages/element-view";

export default () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/home" component={Home} />
      <Route path="/account" component={Account} />
      <Route path="/login" component={Login} />
      <Route path="/antd-view" component={Antd} />
      <Route path="/lifecycles" component={LifeCycles} />
      <Route path="/element-view" component={ElementUi} />
    </Switch>
  );
};
