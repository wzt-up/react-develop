import React, { Component } from "react";
import { Switch, Card } from "antd";
import Home from "../home/index";
import "./index.scss";

export default class Index extends Component {
  constructor(props) {
    console.log("constructor");
    super(props);
    this.state = {
      flag: true,
    };
  }
  componentDidMount() {
    console.log("componentDidMount");
  }
  static getDerivedStateFromProps(props, state) {
    console.log("getDerivedStateFromProps");
    return null;
  }
  shouldComponentUpdate() {
    console.log("shouldComponentUpdate");
    return true;
  }
  getSnapshotBeforeUpdate() {
    console.log("getSnapshotBeforeUpdate");
    return true;
  }
  componentDidUpdate() {
    console.log("componentDidUpdate");
  }
  componentWillUnmount() {
    console.log("componentWillUnmount");
  }
  onChange = () => {
    const { flag } = this.state;
    this.setState({ flag: !flag });
  };
  render() {
    const { flag } = this.state;
    console.log("render");
    return (
      <div>
        <Home></Home>
        <Switch defaultChecked onChange={this.onChange} />
        {flag && (
          <Card title="卡片" bordered={false} style={{ width: 300 }}>
            {[1, 2, 3, 4, 5, 6, 7].map((item, index) => (
              <div key={index}>{item}</div>
            ))}
          </Card>
        )}
      </div>
    );
  }
}
