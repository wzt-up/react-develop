import React, { Component } from "react";
import { Button, Descriptions } from "antd";
import getWeather, { getMusic } from "../../api/index";
import Weather from "../../components/Weather";
import Popup from "../../components/Popup/Popup";
import Home from "../home/index";

export default class Login extends Component {
  constructor(props) {
    super();
    this.state = {
      flag: false,
    };
  }
  componentDidMount() {
    // this.onGetSinglePoetry();
  }
  onGetMusic = () => {
    getMusic('周杰伦');
  };
  onGetSinglePoetry = async () => {
    const code = "110101";
    let res = await getWeather(code);
    this.setState({ poetry: res.data });
  };
  onPopupOpen = () => {
    this.setState({ flag: true });
  };
  onPopupClose = () => {
    this.setState({ flag: false });
  };
  render() {
    const { flag, poetry } = this.state;
    console.log(poetry);

    return (
      <div>
        <Home></Home>
        <Button type="primary" onClick={this.onPopupOpen}>
          点击弹出
        </Button>
        <Popup
          isOpened={flag}
          overlayStyle="right-filter"
          onClose={this.onPopupClose}
        >
          <details>
            <summary>点击打开</summary>
            <div contenteditable="true">内容可编辑</div>
          </details>
          <form>
            <input
              type="text"
              pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$"
              required
            />
            <input type="color" name="" id="" />
            <input type="submit"></input>
            <input
              type="range"
              name="range"
              min="0"
              max="100"
              step="1"
              value=""
              onchange="changeValue(event)"
            />
          </form>
        </Popup>
        <Button type="primary" onClick={this.onGetSinglePoetry}>
          点击获取天气
        </Button>
        <Button type="primary" onClick={this.onGetMusic}>
          点击获取music
        </Button>
        {poetry &&
          poetry.lives &&
          poetry.lives.map((item) => <Weather weather={item}></Weather>)}
      </div>
    );
  }
}
